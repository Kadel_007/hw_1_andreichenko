# Homework 1
# Задача: Создать две переменные first=10, second=30. 
# Вывести на экран результат математического взаимодействия (+, -, *, / и тд.) для этих чисел.

def main(): 
    first = 10
    second = 30
    print("first = ", first)
    print("second = ", second)
    
    print("first + second = ", first + second)
    print("second - first = ", second - first)
    print("first - second = ", first - second)
    print("first * second = ", first * second)
    print("first / second = ", first / second)
    print("second / first  = ", second / first)
    print("first ** second = ", first ** second)
    print("second % first = ", second % first)
    print("first % second = ", first % second)
    print("second // first = ", second // first)
    print("first // second = ", first // second)
    
    

main()   
    